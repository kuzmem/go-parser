package repo

import (
	"fmt"
	"github.com/jackc/pgx/v5"
	"github.com/joho/godotenv"
	"gitlab.com/kuzmem/go-parser/parser/model"
	"gitlab.com/kuzmem/go-parser/parser/mongo"
	"gitlab.com/kuzmem/go-parser/parser/postgres"
	"log"
	"os"
	"path/filepath"
	"strconv"
)

var envPath = filepath.Join("..", ".env")

type Repository struct {
	repo     map[int]model.Vacancy
	storage  postgres.VacancyStorage
	storage1 mongo.MongoDB
}

type Storager interface {
	Create(dto model.Vacancy) error
	GetById(id int) (*model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
	UpdateByID(id int, vacancy model.Vacancy) error
}

func NewStorage(db *pgx.Conn, db1 mongo.MongoDB) *Repository {
	return &Repository{
		repo:     make(map[int]model.Vacancy),
		storage:  *postgres.NewVacancyStorage(db),
		storage1: db1,
	}
}

func (s *Repository) Create(dto model.Vacancy) error {
	err := godotenv.Load(envPath)
	if err != nil {
		fmt.Println(err)
	}
	database := os.Getenv("DB")

	id, err := strconv.Atoi(dto.Identifier.Value)
	if err != nil {
		fmt.Println(err)
		return err
	}
	s.repo[id] = dto
	switch database {
	case "postgres":
		err = s.storage.CreateVacancy(dto)
		if err != nil {
			fmt.Println(err)
		}
	case "mongo":
		err = s.storage1.CreateVacancy(dto)
		if err != nil {
			fmt.Println(err)
			return err
		}
	default:
		log.Fatal("I dont know this database!")
	}

	return nil
}

func (s *Repository) GetById(id int) (*model.Vacancy, error) {
	err := godotenv.Load(envPath)
	if err != nil {
		fmt.Println(err)
	}
	database := os.Getenv("DB")
	switch database {
	case "postgres":
		vacancy, err := s.storage.GetVacancyById(id)
		if err != nil {
			return &model.Vacancy{}, err
		}
		return vacancy, nil
	case "mongo":
		vacancy, err := s.storage1.GetVacancyById(id)
		if err != nil {
			return &model.Vacancy{}, err
		}
		return vacancy, nil
	default:
		log.Fatal("I dont know this database!")
		return &model.Vacancy{}, err
	}

}

func (s *Repository) GetList() ([]model.Vacancy, error) {
	err := godotenv.Load(envPath)
	if err != nil {
		fmt.Println(err)
	}
	database := os.Getenv("DB")
	switch database {
	case "postgres":
		return s.storage.GetAllVacancies()
	case "mongo":
		return s.storage1.GetAllVacancies()
	default:
		log.Fatal("I dont know this database!")
		return nil, err
	}
}

func (s *Repository) Delete(id int) error {
	err := godotenv.Load(envPath)
	if err != nil {
		fmt.Println(err)
	}
	database := os.Getenv("DB")
	switch database {
	case "postgres":
		err = s.storage.DeleteVacancyById(id)
		if err != nil {
			return err
		}
	case "mongo":
		err = s.storage1.DeleteVacancyById(id)
		if err != nil {
			return err
		}
	default:
		log.Fatal("I dont know this database!")
	}

	return nil
}

func (s *Repository) UpdateByID(id int, vacancy model.Vacancy) error {
	err := godotenv.Load(envPath)
	if err != nil {
		fmt.Println(err)
	}
	database := os.Getenv("DB")
	switch database {
	case "postgres":
		err = s.storage.UpdateVacancyById(id, vacancy)
		if err != nil {
			return err
		}
	case "mongo":
		err = s.storage1.UpdateVacancyById(id, vacancy)
		if err != nil {
			return err
		}
	default:
		log.Fatal("I dont know this database!")
	}

	return nil
}
