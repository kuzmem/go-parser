package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/jackc/pgx/v5"
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/firefox"
	"gitlab.com/kuzmem/go-parser/parser/logic"
	"gitlab.com/kuzmem/go-parser/parser/model"
	"gitlab.com/kuzmem/go-parser/parser/mongo"
	"gitlab.com/kuzmem/go-parser/parser/repo"
	"net/http"
)

type VacancyController struct {
	storage repo.Repository
}

func NewVacancyController(db *pgx.Conn, db1 mongo.MongoDB) *VacancyController {
	return &VacancyController{storage: *repo.NewStorage(db, db1)}
}

func (v *VacancyController) SearchVacancy(w http.ResponseWriter, r *http.Request) {
	maxTries := 15
	// прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "firefox",
	}

	// добавляем в конфигурацию драйвера настройки для chrome
	chrCaps := firefox.Capabilities{}
	caps.AddFirefox(chrCaps)

	// переменная нашего веб драйвера
	var wd selenium.WebDriver
	var err error
	// прописываем адрес нашего драйвера
	urlPrefix := selenium.DefaultURLPrefix
	// немного костылей чтобы драйвер не падал
	for i := 0; i < maxTries; {
		wd, err = selenium.NewRemote(caps, urlPrefix)
		if err != nil {
			fmt.Println(err)
			fmt.Println("Failed")
			i++
			continue
		}
		break
	}

	var query map[string]string
	if err := json.NewDecoder(r.Body).Decode(&query); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	Vacancies, err := logic.VacanciesLogic(&wd, query["query"])
	if err != nil {
		fmt.Println(err)
	}

	for _, vacancy := range Vacancies {
		if err := v.storage.Create(vacancy); err != nil {
			fmt.Println(err)
		}
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8

	err = json.NewEncoder(w).Encode(Vacancies) // записываем результат Pet json в http.ResponseWriter
	if err != nil {                            // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	err = wd.Quit()
	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (v *VacancyController) DeleteVacancyById(w http.ResponseWriter, r *http.Request) {
	var query map[string]int
	if err := json.NewDecoder(r.Body).Decode(&query); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := v.storage.Delete(query["id"]); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err := json.NewEncoder(w).Encode("Deleting is successful"); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (v *VacancyController) GetVacancyById(w http.ResponseWriter, r *http.Request) {
	var query map[string]int
	if err := json.NewDecoder(r.Body).Decode(&query); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	Vacancy, err := v.storage.GetById(query["id"])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	err = json.NewEncoder(w).Encode(Vacancy)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (v *VacancyController) GetList(w http.ResponseWriter, r *http.Request) {
	Vacancies, err := v.storage.GetList()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(Vacancies)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}
func (v *VacancyController) UpdateByID(w http.ResponseWriter, r *http.Request) {
	var updateData struct {
		ID      int           `json:"id"`
		Vacancy model.Vacancy // Предположим, что у тебя есть структура Vacancy
	}

	if err := json.NewDecoder(r.Body).Decode(&updateData); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Вызываем метод UpdateByID из хранилища, чтобы обновить вакансию
	err := v.storage.UpdateByID(updateData.ID, updateData.Vacancy) // Укажи правильный путь к файлу .env
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err := json.NewEncoder(w).Encode("Update is successful"); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
