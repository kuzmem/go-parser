package main

import "gitlab.com/kuzmem/go-parser/parser/model"

func init() {
	_ = vacancyDeleteRequest{}
	_ = vacancyDeleteResponse{}
	_ = vacancyGetRequest{}
	_ = vacancyGetResponse{}
	_ = vacancyListRequest{}
	_ = vacancyListResponse{}
	_ = vacancySearchRequest{}
	_ = vacancySearchResponse{}
}

//go:generate swagger generate spec -o ../public/swagger.json --scan-models

// swagger:route POST /search vacancy vacancySearchRequest
// Поиск вакасий
// responses:
// 200: vacancySearchResponse

// swagger:parameters vacancySearchRequest
type vacancySearchRequest struct {
	// required:true
	// in:body
	// example: {"query": "golang"}
	Body Query `json:"body"`
}

// swagger:model Query
type Query struct {
	Query string `json:"query"`
}

// swagger:model Id
type Id struct {
	Id int `json:"id"`
}

// swagger:response vacancySearchResponse
type vacancySearchResponse struct {
	// in:body
	Bodies []model.Vacancy `json:"body"`
}

// swagger:route POST /delete vacancy vacancyDeleteRequest
// Удаление вакасии
// responses:
// 200: vacancyDeleteResponse

// swagger:parameters vacancyDeleteRequest
type vacancyDeleteRequest struct {
	// required:true
	// in:body
	Body Id `json:"id"`
}

// swagger:response vacancyDeleteResponse
type vacancyDeleteResponse struct {
	// in:body
	Body model.Vacancy `json:"body"`
}

// swagger:route POST /get vacancy vacancyGetRequest
// Поиск вакасии по id
// responses:
// 200: vacancyGetResponse

// swagger:parameters vacancyGetRequest
type vacancyGetRequest struct {
	// required:true
	// in:body
	Body Id `json:"id"`
}

// swagger:response vacancyGetResponse
type vacancyGetResponse struct {
	// in:body
	Body model.Vacancy `json:"body"`
}

// swagger:route GET /list vacancy vacancyListRequest
// Список вакасий
// responses:
// 200: vacancyListResponse

// swagger:parameters vacancyListRequest
type vacancyListRequest struct {
}

// swagger:response vacancyListResponse
type vacancyListResponse struct {
	// in:body
	Body []model.Vacancy `json:"body"`
}
