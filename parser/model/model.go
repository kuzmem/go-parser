package model

type Vacancy struct {
	ID            int                `bson:"_id,omitempty"`
	Context       string             `json:"@context"`
	Type          string             `json:"@type"`
	DatePosted    string             `json:"datePosted"`
	Title         string             `json:"title"`
	Description   string             `json:"description"`
	Identifier    Identifier         `json:"identifier"`
	HiringOrg     HiringOrganization `json:"hiringOrganization"`
	JobLocation   JobLocation        `json:"jobLocation"`
	JobLocationID string             `json:"jobLocationType"`
	Employment    string             `json:"employmentType"`
}

type Identifier struct {
	Type  string `json:"@type"`
	Name  string `json:"name"`
	Value string `json:"value"`
}

type HiringOrganization struct {
	Type   string `json:"@type"`
	Name   string `json:"name"`
	SameAs string `json:"sameAs"`
}

type JobLocation struct {
	PlaceType string  `json:"@type"`
	Address   Address `json:"address"`
}

type Address struct {
	AddressType     string  `json:"@type"`
	StreetAddress   string  `json:"streetAddress"`
	AddressLocality string  `json:"addressLocality"`
	AddressCountry  Country `json:"addressCountry"`
}

type Country struct {
	CountryType string `json:"@type"`
	CountryName string `json:"name"`
}
