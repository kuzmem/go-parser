package mongo

import (
	"context"
	"fmt"
	"gitlab.com/kuzmem/go-parser/parser/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoDB struct {
	Client     *mongo.Client
	Collection *mongo.Collection
}

type Storager interface {
	CreateVacancy(vacancy model.Vacancy) error
	GetVacancyById(id int) (*model.Vacancy, error)
	UpdateVacancyById(id int, vacancy model.Vacancy) error
	DeleteVacancyById(id int) error
	GetAllVacancies() ([]model.Vacancy, error)
}

func NewMongoDB(databaseName, collectionName string) (*MongoDB, error) {
	clientOptions := options.Client().ApplyURI("mongodb://user:pass@localhost:27017/mongo1")

	client, err := mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		return nil, err
	}

	database := client.Database(databaseName)
	collection := database.Collection(collectionName)

	return &MongoDB{
		Client:     client,
		Collection: collection,
	}, nil
}

func (db *MongoDB) CreateVacancy(vacancy model.Vacancy) error {
	fmt.Println(vacancy)

	_, err := db.Collection.InsertOne(context.Background(), vacancy)
	if err != nil {
		return err
	}

	fmt.Println("Vacancy created successfully")
	return nil
}

func (db *MongoDB) GetAllVacancies() ([]model.Vacancy, error) {

	cur, err := db.Collection.Find(context.Background(), bson.M{})
	if err != nil {
		return nil, err
	}
	defer cur.Close(context.Background())

	var vacancies []model.Vacancy
	for cur.Next(context.Background()) {
		var vacancy model.Vacancy
		if err := cur.Decode(&vacancy); err != nil {
			return nil, err
		}

		vacancies = append(vacancies, vacancy)
	}

	if err := cur.Err(); err != nil {
		return nil, err
	}

	return vacancies, nil
}

func (db *MongoDB) GetVacancyById(id int) (*model.Vacancy, error) {
	filter := bson.M{"_id": id}

	var result model.Vacancy
	err := db.Collection.FindOne(context.Background(), filter).Decode(&result)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (db *MongoDB) UpdateVacancyById(id int, vacancy model.Vacancy) error {
	filter := bson.M{"_id": id}

	update := bson.M{"$set": vacancy}
	_, err := db.Collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		return err
	}

	fmt.Println("Vacancy updated successfully")
	return nil
}

func (db *MongoDB) DeleteVacancyById(id int) error {
	filter := bson.M{"_id": id}

	_, err := db.Collection.DeleteOne(context.Background(), filter)
	if err != nil {
		return err
	}

	fmt.Println("Vacancy deleted successfully")
	return nil
}
